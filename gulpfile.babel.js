import gulp from 'gulp';
import parcel from 'gulp-parcel';

gulp.task('build', () => gulp.src('src/index.html', {read: false})
    .pipe(parcel({outDir: 'public', hmr: true}))
    .pipe(gulp.dest('public'))
)
