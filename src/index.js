import Vue from 'vue/dist/vue.js'
import Home from '../views/Home.vue'
import Gallery from '../views/Gallery.vue'
import Skill from '../views/Skill.vue'
import SkillList from '../views/SkillList.vue'
import About from '../views/About.vue'
import SectionGallery from '../views/SectionGallery.vue'
import SectionGameAssets from '../views/SectionGameAssets.vue'

import { sections } from '../js/sections'

import '../css/queries.css'
import '../css/gallery.css'
import '../css/main.css'
import '../css/sections/web.css'
import '../css/sections/blender.css'
import '../css/sections/unity.css'
import '../css/sections/unreal.css'
import '../css/sections/game-assets.css'
import '../css/sections/footer.css'
import '../css/section.css'

var data = {
  sections: sections
}

new Vue({
  el: '#body',
  components: {
    Home, Gallery, Skill, SkillList, About, SectionGallery, SectionGameAssets
  },
  data: data,
});
