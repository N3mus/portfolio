import sectionBlenderImage from '../images/blender.png'
import sectionUnityImage from '../images/unity.png'
import sectionUnrealImage from '../images/unreal.png'
import sectionAssetsImage from '../images/game-assets.png'
import aboutWebImage from '../images/web.png'

import blenderSmallImages from '../images/small/*.jpg'
import blenderBigImages from '../images/big/*.jpg'
import unitySmallImages from '../images/unity-small/*.jpg'
import unityBigImages from '../images/unity/*.jpg'
import unrealSmallImages from '../images/unreal-small/*.jpg'
import unrealBigImages from '../images/unreal/*.jpg'
import gameAssetsFerrisWheelSmall from '../images/game-assets-small/ferrisWheel/*.jpg'
import gameAssetsFerrisWheel from '../images/game-assets/ferrisWheel/*.jpg'
import gameAssetsModularFactorySmall from '../images/game-assets-small/modularFactory/*.jpg'
import gameAssetsModularFactory from '../images/game-assets/modularFactory/*.jpg'
import gameAssetsWoodTowerSmall from '../images/game-assets-small/woodTower/*.jpg'
import gameAssetsWoodTower from '../images/game-assets/woodTower/*.jpg'
import gameAssetsWoodBarrelCartSmall from '../images/game-assets-small/woodBarrelCart/*.jpg'
import gameAssetsWoodBarrelCart from '../images/game-assets/woodBarrelCart/*.jpg'

import {skills} from './skills'

const galleries = {
  blender: {
    thumbnails: blenderSmallImages,
    images: blenderBigImages,
  },
  unity: {
    thumbnails: unitySmallImages,
    images: unityBigImages,
  },
  unreal: {
    thumbnails: unrealSmallImages,
    images: unrealBigImages,
  },
  gameAssets: [{
      title: 'Ferris Wheel',
      thumbnails: gameAssetsFerrisWheelSmall,
      images: gameAssetsFerrisWheel,
    }, {
      title: 'Modular Factory',
      unityLink: 'https://assetstore.unity.com/packages/3d/environments/industrial/modular-factory-building-pbr-136573',
      thumbnails: gameAssetsModularFactorySmall,
      images: gameAssetsModularFactory,
    }, {
      title: 'Wood Tower',
      thumbnails: gameAssetsWoodTowerSmall,
      images: gameAssetsWoodTower,
    }, {
      title: 'Wood Barrel Cart',
      unityLink: 'https://assetstore.unity.com/packages/3d/vehicles/land/wooden-barrel-cart-pbr-137573',
      thumbnails: gameAssetsWoodBarrelCartSmall,
      images: gameAssetsWoodBarrelCart,
    }
  ]
}

export const sections = {
    home: {
      title: 'Welcome',
      subtitle: 'check out my portfolio!',
      anchors: [
        {
          name: '3D Graphics',
          anchor: '#section-blender'
        },
        {
          name: 'Visualizations',
          anchor: '#section-unity'
        },
        {
          name: 'Animations',
          anchor: '#section-unreal'
        },
        {
          name: 'Game Assets',
          anchor: '#section-assets'
        }
      ],
      icons: [
        {
          icon: 'facebook',
          href: 'https://www.facebook.com/maciej.suszko'
        },
        {
          icon: 'deviantart',
          href: 'http://nemusnem.deviantart.com'
        },
        {
          icon: 'youtube',
          href: 'https://www.youtube.com/channel/UC4rFpcybjB7hknrPKB6Te1Q'
        },
        {
          icon: 'linkedin',
          href: 'https://linkedin.com/in/maciej-suszko-b55340134'
        },
        {
          icon: 'bitbucket',
          href: 'https://bitbucket.org/N3mus/portfolio/src/master/'
        },
      ]
    },
    userWeb: {
      class: 'section_web',
      background: aboutWebImage,
      backgroundClass: 'section_web_image',
      title: 'Web Developer since 2016.',
      skills: skills,
    },
    blender: {
      class: 'section_blender',
      gallery: galleries.blender,
      background: sectionBlenderImage,
      backgroundClass: 'section_blender_image',
      title: '3D Graphics',
      subtitle: 'using Blender',
      anchor: 'section-blender',
      descriptions: [
        'My compositions based on <span class="section_desc-highlight">Cycles Render Engine</span>.',
        'I like to create fantasy scenes, exteriors, landscapes, interiors.'
      ]
    },
    unity: {
      class: 'section_unity',
      gallery: galleries.unity,
      background: sectionUnityImage,
      backgroundClass: 'section_unity_image',
      title: 'Visualizations',
      subtitle: 'using Game Engines',
      anchor: 'section-unity',
      descriptions: [
        `I'm familiar with <span class="section_desc-highlight">Unity3D</span> and <span class="section_desc-highlight">Unreal Engine</span>.`,
        `I like to create <span class="section_desc-highlight">PHOTOREALISTIC</span> visualizations with interaction possibility.`,
        `I know Unity environment very well and <span class="section_desc-highlight">PHYSICALLY BASED RENDERING</span> model.`
      ]
    },
    unreal: {
      class: 'section_unreal',
      gallery: galleries.unreal,
      background: sectionUnrealImage,
      backgroundClass: 'section_unreal_image',
      title: 'Animations',
      subtitle: 'using Unreal Engine',
      anchor: 'section-unreal',
      descriptions: [
        `I can create <span class="section_desc-highlight">REALTIME</span> animations based on PBR model.`,
        `Any model needed for animation I can just create using Blender.`
      ],
      movies: [
        'https://www.youtube.com/embed/82hTb1ngHVc',
        'https://www.youtube.com/embed/wDkMqvO09TA'
      ]
    },
    gameAssets: {
      class: 'section_assets',
      galleries: galleries.gameAssets,
      background: sectionAssetsImage,
      backgroundClass: 'section_assets_image',
      title: 'Game Assets',
      subtitle: 'for Unreal Engine, Unity3D',
      anchor: 'section-assets',
      descriptions: [
        'I can model and texture <span class="section_desc-highlight">GAME ASSETS</span> in PBR.',
        'I love create procedural models.'
      ]
    }
  }