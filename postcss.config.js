module.exports = {
  "modules": false,
  "plugins": {
    "postcss-import": true,
    "postcss-nested": true,
    "postcss-mixins": true,
    "postcss-custom-media": true,
    "postcss-custom-properties": true,
    "autoprefixer": {
      "grid": true
    }
  }
}
// https://github.com/parcel-bundler/parcel/issues/329#issuecomment-365082690